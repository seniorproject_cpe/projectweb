<?php
include("tcpdf/tcpdf.php");
date_default_timezone_set('Asia/Bangkok');
$data = json_decode(file_get_contents('php://input'), true);
print_r($data);
// echo $data['napart'];
// echo $data['list']['power']['unit'];
// <tr align="center">
//  <th >ลำดับ</th>
//  <th colspan="3">รายการ</th>
//  <th >จำนวน</th>
//  <th >ราคา</th>
//  <th >รวมเงิน</th>
// </tr>
$summary = 0;
$loop = 1;
$innerhtml = "";
foreach ($data['list'] as $key => $value) {
    $innerhtml .= '<tr align="center">';
	$innerhtml .= '<td>'.$loop.'</td>';
    $temp = $data['list'][$key];
    $innerhtml .= '<td colspan="3">'.$key.'</td>';
    $innerhtml .= '<td>'.$temp['use'].'</td>';
    $innerhtml .= '<td>'.$temp['unit'].'</td>';
    $innerhtml .= '<td>'.$temp['use']*$temp['unit'].'</td>';
    $summary += $temp['use']*$temp['unit'];
    //echo $key."<br>"; // เอาชื่อมันออกมา
    // foreach ( $temp as $key => $value)
    // {
    //     //echo $key." : ".$temp[$key]."<br>"; 
        
    // }
    //echo $temp['use']*$temp['unit']."<br>";
	$loop++;
    $innerhtml .= '</tr>';
}




$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Skyview');
$pdf->SetTitle('ใบแจ้งหนี้');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData('', 1, 'Cloud Serviced Apartment Management System', $data['napart']);

// set header and footer fonts
$pdf->setHeaderFont(Array('thsarabun', '', 18));
$pdf->setFooterFont(Array('thsarabun', '', 18));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font

$pdf->SetFont('thsarabun', '', 14, '', true);
// Add a page
// This method has several options, check the source code documentation for more information.
$namemonth = date("F");
$thai_month_arr=array(
    "0"=>"",
    "1"=>"มกราคม",
    "2"=>"กุมภาพันธ์",
    "3"=>"มีนาคม",
    "4"=>"เมษายน",
    "5"=>"พฤษภาคม",
    "6"=>"มิถุนายน",
    "7"=>"กรกฎาคม",
    "8"=>"สิงหาคม",
    "9"=>"กันยายน",
    "10"=>"ตุลาคม",
    "11"=>"พฤศจิกายน",
    "12"=>"ธันวาคม"
);
function thai_date(){
    global $thai_day_arr,$thai_month_arr;
    $thai_date_return="เดือน ".$thai_month_arr[date("n")];
    $thai_date_return.= " พ.ศ.".(date("Y")+543);
    return $thai_date_return;
}

$namecus = $data['ncus'];
$addresscus = $data['address'];
$nobill = $data['billorder'];
$roomnumber = $data['room'];
$pdf->AddPage();
ob_start();
// Set some content to print
$htmlq = <<<EOD
<table>
<tr><td colspan="5"></td><td>วันที่</td><td width="20%">22 เมษายน 2560</td></tr>
<tr><td><b>ชื่อ</b></td><td colspan="4">$namecus</td><td><b><center>เลขที่บิล</center></b></td><td>$nobill</td></tr>
<tr><td><b>ที่อยู่</b></td><td colspan="<4></4>">$addresscus</td><td><b>ห้อง</b></td><td >$roomnumber</td></tr>
</table>
<br>
<br>
<br>
<table border = "1">
<thead>
<tr align="center">
 <th >ลำดับ</th>
 <th colspan="3">รายการ</th>
 <th >จำนวน</th>
 <th >ราคา</th>
 <th >จำนวนเงิน</th>
</tr>
</thead>
<tbody>
$innerhtml
<tr align="center">
	<td  colspan="6">รวมเงินทั้งสิ้น</td>
	<td >$summary</td>
</tr>
</tbody>
</table>
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $htmlq, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
ob_clean();
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_065.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+



?>
