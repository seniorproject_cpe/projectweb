#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import jinja2
from jinja2 import Environment, PackageLoader
import json
from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.api import images
from google.appengine.api import app_identity
from google.appengine.api import mail

import os
import urllib
import hashlib
import requests
from datetime import datetime, date, time, timedelta
import string
import time
import random
import uuid
import sys
import appengine_config
import jwt
from admin import AdminAccount
from Customer import Customer
from Customer import Customer_Token
from ApartmentConsole import Apartment
from ApartmentConsole import List_serviceRoom
from ApartmentConsole import List_userinroom
from ApartmentConsole import List_adminControl
from ApartmentConsole import List_submitpayment
from ApartmentConsole import List_servicepermonth
from ApartmentConsole import List_sumpaymentroom



from Report import FixReport

from Ledger import Ledger
from md5 import md5

from webapp2_extras import sessions
from webapp2_extras import auth
from webapp2_extras.auth import InvalidAuthIdError
from webapp2_extras.auth import InvalidPasswordError

from datetime import datetime

from basehandler import BaseHandler
from xhtml2pdf import pisa
from cStringIO import StringIO
reload(sys)
sys.setdefaultencoding("utf-8")

JINJA_ENVIRONMENT = jinja2.Environment(
	loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
	extensions=['jinja2.ext.autoescape','jinja2.ext.loopcontrols'],
	autoescape=True)
JINJA_ENVIRONMENT.globals['sessionapartid'] = ""
index = JINJA_ENVIRONMENT.get_template('/templates/main.html')
dashboard = JINJA_ENVIRONMENT.get_template('/templates/login.html')
regis = JINJA_ENVIRONMENT.get_template('/templates/regis.html')
consoleadmin = JINJA_ENVIRONMENT.get_template('/templates/consoleadmin.html')
genqr = JINJA_ENVIRONMENT.get_template('/templates/genstring.html')
newroom = JINJA_ENVIRONMENT.get_template('/templates/newroom.html')
regiscustomer = JINJA_ENVIRONMENT.get_template('/templates/regiscustomer.html')
moveroom = JINJA_ENVIRONMENT.get_template('/templates/moveroom.html')
suadmin = JINJA_ENVIRONMENT.get_template('/templates/superadmin.html')
mdash = JINJA_ENVIRONMENT.get_template('/templates/maindashboard.html')
newservice = JINJA_ENVIRONMENT.get_template('/templates/newservice.html')
pagefix = JINJA_ENVIRONMENT.get_template('/templates/reportfix.html')
testfb = JINJA_ENVIRONMENT.get_template('/templates/firebaseindex.html')

class QRcodegen(ndb.Model):
	id_user = ndb.StringProperty(required=True)
	string = ndb.StringProperty(required=True)
	hashcheck = ndb.StringProperty(required=True)
	timeexp = ndb.DateTimeProperty(auto_now_add=True)

class Service(ndb.Model):
	id = ndb.StringProperty(required=True)
	name = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(required=True)
	price = ndb.FloatProperty(default = 0)
	status = ndb.IntegerProperty(default = 1)
	type =  ndb.IntegerProperty(required=True)


class Translog(ndb.Model):
	action = ndb.StringProperty(required=True)
	lastupdate = ndb.DateTimeProperty(auto_now_add=True)



class Room(ndb.Model):
	"""docstring for Room"""
	id = ndb.StringProperty(required=True)
	numroom = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(required=True)
	used = ndb.IntegerProperty(default=0)
	maxused = ndb.IntegerProperty(default=2)
	floor = ndb.IntegerProperty(required=True)
	status = ndb.StringProperty(default="1")
	rent = ndb.IntegerProperty(default=0)
	submit_payment = ndb.IntegerProperty(default=0)

class paymentmonth(ndb.Model):
	in_apartmentid = ndb.StringProperty(required=True)
	month = ndb.IntegerProperty()
	year = ndb.IntegerProperty()

class dbpic(ndb.Model):
	id = ndb.StringProperty(required=True)
	image = ndb.BlobProperty()


class MyJsonEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, float):
			return format(obj, '.2f')
		if isinstance(obj, unicode):
			return obj
		if isinstance(obj, datetime):
			obj += timedelta(hours=7)
			return obj.strftime("%Y-%m-%d %H:%M:%S")
		return json.JSONEncoder.default(self, obj)


"""a = json.dumps({'user':[p.to_dict() for p in Customer().query().fetch()]},cls=MyJsonEncoder)
self.response.headers['Content-Type'] = 'application/json'
self.response.out.write(a)
datetime.strptime('11/04/2014', '%m/%d/%Y')"""
def user_required(handler):
  """
	ถ้ามี session ก็จะ เข้าได้
	Decorator that checks if there's a user associated with the current session.
	Will also fail if there's no session present.
  """
  def check_login(self, *args, **kwargs):
	auth = self.auth
	if not auth.get_user_by_session():

	  self.redirect(self.uri_for('loginpage'), abort=True)
	else:
	  return handler(self, *args, **kwargs)

  return check_login

class testinput(BaseHandler):
	"""docstring for testinput"""
	"""
	Profile = Profile_users.query().filter(Profile_users.in_apartmentid == "1")
	p = Profile.get()
	self.response.headers['Content-Type'] = 'image/png'
	self.response.out.write(p.picid)

	t = str(int(time.time()))
	d = datetime.utcnow()
	y = str(d.strftime("%Y"))
	m = str(d.strftime("%m"))
	day = str(d.strftime("%d"))

	self.response.headers['Content-Type'] = 'text/html'
	self.response.out.write(day+m+y+t)


	a = json.dumps({'user':[p.to_dict() for p in Customer.query().filter(Customer.users == "test111").fetch()]},cls=MyJsonEncoder)
	self.response.headers['Content-Type'] = 'application/json'
	self.response.out.write(a)


	qrymonth = paymentmonth.query().fetch(1)
	for i in qrymonth:
		month_check = i.month
	d = datetime.utcnow()
	y = str(d.strftime("%Y"))
	timestart = y+"/"+str(month_check-1)+"/"+'24'+" 17:00:00"
	timeend = y+"/"+str(month_check)+"/"+'8'+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
	datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
	dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')
	param = self.request.get('id')
	qrysubmitpay =List_submitpayment.query().filter(ndb.AND(List_submitpayment.timestamp > datestart),(List_submitpayment.timestamp <= dateend)).filter(List_submitpayment.in_apartmentid == param).fetch()

	a = json.dumps({'Test':[p.to_dict() for p in qrysubmitpay]},cls=MyJsonEncoder)
	self.response.headers['Content-Type'] = 'application/json'
	self.response.out.write(a)
	"""
	def get(self):
		userid = self.request.get('id')
		qrycus = Customer.query().filter(Customer.id == userid).fetch()
		for i in qrycus:
			self.response.out.write(i.money)
			break





def genid():
	t = str(int(time.time()))
	d = datetime.utcnow()
	y = str(d.strftime("%Y"))
	m = str(d.strftime("%m"))
	day = str(d.strftime("%d"))
	return day+m+y+t

def time_difference(datetime_time):
	delta = datetime.now() - datetime_time
	return int(delta.total_seconds())

class LoginPage(BaseHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		self.response.write(dashboard.render())


def random_generator(size=6, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for x in range(size))
#WEB Supder Admin
class superadmin(BaseHandler):
	def get(self):
		self.response.write(suadmin.render())
	def post(self):
		data = self.request
		nameapart = data.get('nameapartment')
		description = data.get('description')
		iname = data.get_all('name')
		iuser = data.get_all('users')
		ipass = data.get_all('password')
		apartid= genid()
		AP = Apartment()
		AP.id = apartid
		AP.name = nameapart
		AP.datepay = data.get('datepay')
		AP.description = description
		AP.telephone = "0880026265"
		AP.put()

		SV = Service()
		SV.id = genid()
		SV.name = "ค่าไฟ"
		SV.in_apartmentid = apartid
		SV.type = 0
		SV.put()
		time.sleep(1)
		SV = Service()
		SV.id = genid()
		SV.name = "ค่าน้ำ"
		SV.in_apartmentid = apartid
		SV.type = 0
		SV.put()

		ls = List_servicepermonth()
		ls.id = genid()
		ls.in_apartmentid = apartid
		ls.name = "ค่าไฟ"
		ls.put()
		time.sleep(1)
		ls = List_servicepermonth()
		ls.id = genid()
		ls.in_apartmentid = apartid
		ls.name = "ค่าน้ำ"
		ls.put()
		for i in range (0,len(iname)):
			unique_properties = ['email_address']
			user_data = self.user_model.create_user(iuser[i],unique_properties,email_address=iuser[i],password_raw=ipass[i],phone="phone", verified=False)
			user = user_data[1]
			user_id = user.get_id()
			token = self.user_model.create_signup_token(user_id)
			if not user_data[0]and(user_data[1][0]=="auth_id" or user_data[1][1]=="auth_id"):
				self.response.out.write("user error")
				return
			if not user_data[0]and(user_data[1][0]=="email_address" or user_data[1][1]=="email_address"):
				self.response.out.write("email error")
				return

			LA = List_adminControl()
			LA.in_apartmentid = apartid
			LA.username = iname[i]

			ADP = AdminAccount()
			ADP.users = iuser[i]
			hashmd5 = hashlib.md5(ipass[i])
			ADP.password = hashmd5.hexdigest()
			time.sleep(1)
			ADP.id = genid()
			ADP.in_apartmentid = apartid
			ADP.name = iname[i]
			ADP.typeuser = "1"

			LA.put()
			ADP.put()



		d = datetime.utcnow()
		m = int(d.strftime("%m"))
		y = int(d.strftime("%Y"))
		pm = paymentmonth()
		pm.in_apartmentid = apartid
		pm.month = m
		pm.year = y
		pm.put()
		self.response.headers['Content-Type'] = 'text/html'
		self.response.out.write("PASS")
#WEB ADMIN REGIS
class regiscus(webapp2.RequestHandler):
	"""docstring for genstring"""
	def get(self):
		apartmentid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartmentid
		self.response.headers['Content-Type'] = 'text/html'
		self.response.write(regiscustomer.render())
class Register(webapp2.RequestHandler):
	"""docstring for Register"""
	def get(self):
		apartmentid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartmentid
		qry = Room.query().order(Room.numroom).filter(Room.in_apartmentid == apartmentid).filter(Room.status == "1").fetch()

		value = {"room":qry}
		if(self.request.get('error')=='1'):
			self.response.headers['Content-Type'] = 'text/html'
			data = {"error":"1","room":qry}
			self.response.write(regis.render(data))

		elif(self.request.get('error')=='2'):
			self.response.headers['Content-Type'] = 'text/html'
			data = {"error":"2","room":qry}
			self.response.write(regis.render(data))

		elif(self.request.get('error')=='PASS'):

			data = {"error":"PASS","room":qry}
			self.response.write(regis.render(data))
		else:
			self.response.headers['Content-Type'] = 'text/html'
			self.response.write(regis.render(value))
class Regisaccount(webapp2.RequestHandler):

	def post(self):
		apartmentid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartmentid
		data = self.request
		user = data.get('username')
		qry = Customer.query().filter(Customer.users == user)
		check = 0
		for i in qry:
			if i.users == user:
				self.redirect('/regis?error=1');
				check = 1
				break
		if data.get('repassword') == data.get('password') and (check == 0) and (data.get('repassword') != "" and data.get('password') != ""):
			ac = Customer()

			pw = data.get('password')
			hashmd5 = hashlib.md5(pw)
			ac.in_apartmentid = apartmentid
			selecroom = data.get('seroom')

			ac.id = data.get('id')
			ac.users = user
			ac.password = hashmd5.hexdigest()
			ac.in_roomid = selecroom
			ac.in_apartmentid = apartmentid
			ac.fname = data.get('fname')
			ac.lname = data.get('lname')
			ac.nname = data.get('nname')
			ac.tel = data.get('tel')
			ac.address = data.get('address')
			dob = datetime(int(data.get('year')),int(data.get('month')),int(data.get('day')))
			ac.birth = dob
			ac.money = 0
			tempid = genid()

			if(data.get('picid') != "null" ):
				avatar = data.get('picid')
				avatar = images.resize(avatar, 500,300)

				datapic = dbpic()
				datapic.id = tempid
				datapic.image = avatar
				datapic.put()
				ac.urlpic = "http://seniorproject-1234.appspot.com/querypic?id="+tempid
			else:
				ac.urlpic = ""
			ac.put()

			listroom = List_userinroom()
			listroom.username = user
			listroom.in_apartmentid = apartmentid
			listroom.in_roomid = selecroom
			listroom.put()

			qryroom = Room.query().filter(Room.id == selecroom)
			updateroom = qryroom.get()
			updateroom.used += 1
			updateroom.put()
			self.redirect('/regis?error=PASS');



		elif(check==1):
			self.redirect('/regis?error=1');
		else:
			self.redirect('/regis?error=2');
	def get(self):
		username = self.request.get('username')
		qryuser = Customer.query().filter(Customer.users == username).fetch()
		if (len(qryuser) == 0):
			self.response.out.write("PASS")
		else:
			self.response.out.write("NO")
#WEB ADMIN MAIN
class maindashboard(webapp2.RequestHandler):
	"""docstring for maindashboard."""
	def get(self):
		idapart = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = idapart

		qryroom = Room.query().filter(Room.in_apartmentid == apartid ).filter(Room.status ==  "1").filter(Room.used ==0).fetch()
		qrycus = Customer.query().filter(ndb.AND(Customer.in_apartmentid == apartid,Customer.ready == "1")).fetch()
		qryser = Service.query().filter(Service.in_apartmentid == apartid).fetch()
		qryfix = FixReport.query().filter(ndb.AND(FixReport.in_apartmentid == apartid,FixReport.status == 0)).fetch()
		data = {'room_q':qryroom,'cus_q':qrycus,'ser_q':qryser,'fix_q':qryfix}

		self.response.headers['Content-Type'] = 'text/html'
		self.response.write(mdash.render(data))
class Login(BaseHandler):
	def post(self):
		user = self.request.get("username")
		password = self.request.get("password")
		try:
			u = self.auth.get_user_by_password(user, password, remember=False,save_session=True)
			self.redirect(self.uri_for('main'))
			#self.response.out.write(u)
		except (InvalidAuthIdError, InvalidPasswordError) as e:
			self.redirect(self.uri_for('loginpage'))
class Dashboard(BaseHandler):
	@user_required
	def get(self):
		getuser = self.user_info
		user = getuser['email_address']
		qry = AdminAccount.query().filter(AdminAccount.users == user)
		check =0

		for i in qry:
			if i.users == user :
				iduser = i.id
				apartid = i.in_apartmentid
				name = i.name
				"""logs = Translog()
				logs.action = "Login admin username: "+user
				logs.put()"""

				check = 1
				break

			else:
				check =0

		if check == 0:
			self.redirect('/')
		else:
			JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
			arraymonth = ["","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"]
			qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == apartid).fetch(1)
			for i in qrymonth:
				month_check = i.month
			d = datetime.utcnow()
			y = str(d.strftime("%Y"))
			month = int(str(d.strftime("%m")))
			qrydatepay = Apartment.query(Apartment.id == apartid).fetch()
			for itemdate in qrydatepay:
				datepay = itemdate.datepay
			if(int(month_check) == 1):
				timestart = str(int(y)-1)+"/"+str(12)+"/"+str(datepay)+" 17:00:00"
			else:
				timestart = y+"/"+str(int(month_check)-1)+"/"+str(datepay)+" 17:00:00"
			timeend = y+"/"+str(month_check)+"/"+str(datepay)+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
			datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
			dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')


			qry  = List_servicepermonth.query().filter(List_servicepermonth.in_apartmentid == apartid).fetch()
			# data = {"func":func,"qrylist":qry,"month":arraymonth[month_check],"apartid":apartid,"qryled":qryled}
			qryroom = Room.query().filter(Room.in_apartmentid == apartid ).filter(Room.status ==  "1").filter(Room.used ==0).fetch()
			qrycus = Customer.query().filter(ndb.AND(Customer.in_apartmentid == apartid,Customer.ready == "1")).fetch()
			qryser = Service.query().filter(Service.in_apartmentid == apartid).fetch()
			qryfix = FixReport.query().filter(ndb.AND(FixReport.in_apartmentid == apartid,FixReport.status == 0)).fetch()
			qrychecksubmit = Room.query().filter(Room.in_apartmentid == apartid).filter(Room.submit_payment == 0).filter(Room.used > 0).fetch()
			datetimes = datetime.utcnow() + timedelta(hours=7)
			day = int(datetimes.strftime("%d"))
			if((day>= 20 and day<25)):
				data = {"iduser":iduser,"name":name,"id":apartid,'room_q':qryroom,'cus_q':qrycus,'ser_q':qryser,'fix_q':qryfix,'pay_day':'YES','check_submit':qrychecksubmit}
			else:
				data = {"iduser":iduser,"name":name,"id":apartid,'room_q':qryroom,'cus_q':qrycus,'ser_q':qryser,'fix_q':qryfix,'pay_day':'NO','check_submit':qrychecksubmit}

			self.response.headers['Content-Type'] = 'text/html'
			self.response.write(index.render(data))
class changepass(BaseHandler):

	def post(self):
		apartmentid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartmentid
		adminid = self.request.get('adminid')
		password = self.request.get('newpass')
		old_token = self.request.get('oldpass')
		confirmpass = password
		#self.request.get('confirm_password')
		if not password or password != confirmpass:
			self.response.out.write("Old Password not Correct")
			return
		user = self.user
		user.set_password(password)
		user.put()
		self.response.out.write("Change Password Success")


class Consoleadmin(webapp2.RequestHandler):
	"""docstring for tabservice"""
	def post(self):
		func = self.request.get("function")
		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		if(func == "room" ):
			qry = Room.query().order(Room.floor).filter(Room.in_apartmentid == apartid ).fetch()
			qryuser = List_userinroom.query().filter(List_userinroom.in_apartmentid == apartid).fetch()
			data = {"func":func,"apartid":apartid,"qryroom":qry,"qryuser":qryuser}
		elif(func == "cus"):
			qry = Room.query().filter(Room.in_apartmentid == apartid ).fetch()
			qryuser = Customer.query().order(Customer.users).filter(Customer.in_apartmentid == apartid and Customer.ready == "1").fetch()
			data = {"func":func,"apartid":apartid,"qryroom":qry,"qryuser":qryuser}
		elif(func == "pay_day"):
			try:
				qry = Room.query().filter(Room.in_apartmentid == apartid ).filter(Room.used > 0).fetch()
				qryser = Service.query().filter(Service.in_apartmentid == apartid).filter(Service.status == 1).filter(Service.type != 2).filter(Service.type != 3).fetch()
				qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == apartid).fetch(1)
				for i in qrymonth:
					month_check = i.month
			except:
				self.redirect('/')
			qrydatepay = Apartment.query(Apartment.id == apartid).fetch()
			for itemdate in qrydatepay:
				datepay = itemdate.datepay
			d = datetime.utcnow()
			y = str(d.strftime("%Y"))
			if(int(month_check) == 1):
				timestart = str(int(y)-1)+"/"+str(12)+"/"+str(datepay)+" 17:00:00"
			else:
				timestart = y+"/"+str(int(month_check)-1)+"/"+str(datepay)+" 17:00:00"
			timeend = y+"/"+str(month_check)+"/"+'24'+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
			datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
			dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')
			qrysubmitpay = List_submitpayment.query().filter(List_submitpayment.in_apartmentid == apartid).filter(ndb.AND(List_submitpayment.timestamp > datestart),(List_submitpayment.timestamp <= dateend)).fetch()
			#qrysubmitpay = List_submitpayment.query().filter(List_submitpayment.in_apartmentid == apartid)
			data = {"func":func,"apartid":apartid,"qryroom":qry,"qryser":qryser,"listpayment":qrysubmitpay}
		elif(func == "profile"):
			iduser = self.request.get("iduser")
			qryadmin = AdminAccount.query().filter(AdminAccount.id == iduser).fetch(1)
			data = {"func":func,"apartid":apartid,'qryadmin':qryadmin}
		elif(func == "service"):
			qry = Service.query().filter(Service.in_apartmentid == apartid).fetch()
			data = {"func":func,"apartid":apartid,"qrysevice":qry}
		elif(func == "accountmonth" or func == "accountmonthedit"):
			arraymonth = ["","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"]
			qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == apartid).fetch(1)
			for i in qrymonth:
				month_check = i.month
			d = datetime.utcnow()
			y = str(d.strftime("%Y"))
			month = int(str(d.strftime("%m")))
			qrydatepay = Apartment.query(Apartment.id == apartid).fetch()
			for itemdate in qrydatepay:
				datepay = itemdate.datepay
			if(int(month_check) == 1):
				timestart = str(int(y)-1)+"/"+str(12)+"/"+str(datepay)+" 17:00:00"
			else:
				timestart = y+"/"+str(int(month_check)-1)+"/"+str(datepay)+" 17:00:00"
			timeend = y+"/"+str(month_check)+"/"+str(datepay)+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
			datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
			dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')
			qryled= Ledger.query().filter(Ledger.in_apartmentid == apartid).filter(Ledger.type == "OUTCOME").filter(ndb.AND(Ledger.timestamp > datestart,Ledger.timestamp <= dateend)).fetch()
			# qryoutcome= Ledger.query().filter(Ledger.in_apartmentid == apartid).filter(ndb.AND(Ledger.type == "OUTCOME",Ledger.timestamp > datestart,Ledger.timestamp <= dateend)).fetch()
			qryincome= Ledger.query().filter(Ledger.in_apartmentid == apartid).filter(Ledger.type == "INCOME").filter(ndb.AND(Ledger.timestamp > datestart,Ledger.timestamp <= dateend)).fetch()
			qry  = List_servicepermonth.query().filter(List_servicepermonth.in_apartmentid == apartid).fetch()
			data = {"func":func,"qrylist":qry,"month":arraymonth[month_check],"apartid":apartid,"qryled":qryled,"qryin":qryincome}
		elif(func == "invoice"):
			qryroom = Room.query().filter(Room.in_apartmentid == apartid).filter(Room.status == "1").filter(Room.used > 0).fetch()
			data = {"func":func,"roomlist":qryroom}


		self.response.headers['Content-Type'] = 'text/html'
		self.response.write(consoleadmin.render(data))
class NewRooms(webapp2.RequestHandler):

	def get(self):
		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		data = {"id":apartid}
		self.response.headers['Content-Type'] = 'text/html'
		self.response.write(newroom.render(data))
	def post(self):
		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		param = self.request
		room = Room()
		room.id = genid()
		room.numroom = param.get("roomid")
		room.floor = int(param.get("floor"))
		room.maxused = int(param.get("maxuse"))
		room.in_apartmentid = apartid
		room.rent = int(param.get("rent"))
		room.put()
class showpic(webapp2.RequestHandler):
	"""docstring for showpic."""
	def get(self):
		id = self.request.get("id")
		qry = dbpic.query().filter(dbpic.id == id)
		urlpic = qry.get()
		self.response.headers['Content-Type'] = 'image/png'
		self.response.out.write(urlpic.image)
class actioncus(webapp2.RequestHandler):
	def post(self):
		request = self.request
		action = request.get('action')
		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		if(action == "e"):
			newfname = request.get('newfname')
			newnname = request.get('newnname')
			newadd = request.get('newadd')
			newlname = request.get('newlname')
			newtel = request.get('newtel')
			month = request.get('month')
			day = request.get('day')
			year = request.get('year')
			cusid = request.get('cusid')
			qry = Customer.query().filter(Customer.in_apartmentid == apartid and Customer.id == cusid)
			qryget = qry.get()
			qryget.fname = newfname
			qryget.lname = newlname
			qryget.nname = newnname
			qryget.address = newadd
			qryget.tel = newtel
			dob = datetime(int(year),int(month),int(day))
			qryget.birth = dob
			qryget.put()
		elif(action == "m"):
			oldroom = request.get('oldroom')
			newroom = request.get('newroom')
			usercur = request.get('username')
			idcur = request.get('cusid')
			qrylistr = List_userinroom.query().filter(List_userinroom.in_roomid== oldroom).filter(List_userinroom.username == usercur).fetch()
			for i in qrylistr:
				i.in_roomid = newroom
				i.put()
			qrycus = Customer.query().filter(Customer.in_apartmentid == apartid and Customer.in_roomid == oldroom).filter(Customer.id == idcur).fetch()
			for i in qrycus:
				username = i.users
				i.in_roomid = newroom
				i.put()

			qryser = List_serviceRoom.query().filter(List_serviceRoom.user == username)
			for i in qryser:
				i.key.delete()

			qryoldroom = Room.query().filter(Room.in_apartmentid == apartid and Room.id == oldroom).fetch()
			for i in qryoldroom:
				i.used -= 1
				i.put()

			qrynewroom = Room.query().filter(Room.in_apartmentid == apartid and Room.id == newroom).fetch()
			for i in qrynewroom:
				i.used += 1
				i.put()
		elif(action == "money"):
			cusid = request.get('cusid')
			money = request.get('money')
			qrycus = Customer.query().filter(Customer.id == cusid).fetch()
			for i in qrycus:
				lg = Ledger()
				lg.in_apartmentid = i.in_apartmentid
				lg.id = genid()
				lg.money = float(money)
				lg.description = "เติมเงินจาก "+i.users
				lg.type = "INCOME"
				lg.put()
				i.money += float(money)
				i.put()

		elif(action == "repass"):
			cusid = request.get('cusid')
			qrycus = Customer.query().filter(Customer.id == cusid).fetch()
			for i in qrycus:
				hashmd5 = hashlib.md5('123456')
				i.password = hashmd5.hexdigest()
				i.put()

		elif(action == "delete"):
			cusid = request.get('cusid')
			qrycus = Customer.query().filter(Customer.id == cusid)
			for i in qrycus:
				username = i.users
				in_roomid = i.in_roomid
				i.key.delete()
			qrylistuser = List_userinroom.query().filter(List_userinroom.in_roomid == in_roomid).filter(List_userinroom.username == username)
			for i in qrylistuser:
				i.key.delete()
			qryroom = Room.query().filter(Room.id == in_roomid).fetch()
			for i in qryroom:
				i.used -= 1
				i.put()

			qryser = List_serviceRoom.query().filter(List_serviceRoom.user == username)
			for i in qryser:
				i.key.delete()
class actionroom(webapp2.RequestHandler):
	"""docstring for showpic."""
	def post(self):
		param = self.request
		action = param.get('action')
		nroom = param.get('roomid');
		idapart = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = idapart
		if(action == 'c'):
			qry = Room.query().filter(Room.in_apartmentid == idapart and Room.id == nroom)
			resultroom = qry.get()
			if(resultroom.status == "1"):
				resultroom.status = "0"
			else:
				resultroom.status = "1"
			resultroom.put()

		elif(action == 'e'):
			oldroom = param.get('oldroom')
			idapart = param.get('idapart')
			action = param.get('action')
			newroom = param.get('newroom')
			newfloor = int(param.get('newfloor'))
			newrent = int(param.get('newrent'))
			newmax = int(param.get('newmax'))

			qry = Room.query().filter(Room.in_apartmentid == idapart and Room.id == oldroom)
			resultroom = qry.get()
			resultroom.maxused = newmax
			resultroom.numroom = newroom
			resultroom.floor = newfloor
			resultroom.rent = newrent
			resultroom.put()
		elif(action == 'm'):
			oldroom = param.get('oldroom')
			newroom = nroom
			qryuinr = List_userinroom.query().filter(List_userinroom.in_apartmentid == idapart and List_userinroom.in_roomid == oldroom).fetch()
			for i in qryuinr:
				i.in_roomid = newroom
				i.put()

			qryupdateoldroom = Room.query().filter(Room.in_apartmentid == idapart and Room.id == oldroom)
			for i in qryupdateoldroom:
				# getroom = qryupdateoldroom.get()
				tmpoldroom = i.used
				i.used = 0
				i.put()

			qryupdatenewroom = Room.query().filter(Room.in_apartmentid == idapart and Room.id == newroom)
			for i in qryupdatenewroom:

			#getnroom = qryupdatenewroom.get()
				i.used = tmpoldroom
				i.put()

			qrycutomer = Customer.query().filter(Customer.in_apartmentid == idapart and Customer.ready == "1" and Customer.in_roomid == oldroom).fetch()
			for i in qrycutomer:
				username = i.users
				i.in_roomid = newroom
				i.put()

			qryser = List_serviceRoom.query().filter(List_serviceRoom.user == username)
			for i in qryser:
				i.key.delete()
		elif(action == "submit_payment"):
			qryser = Service.query().filter(Service.in_apartmentid == idapart).filter(Service.status == 1).filter(Service.type != 2).filter(Service.type != 3).fetch()
			for i in qryser:
				listsub = List_submitpayment()
				keydata = i.id
				if(i.type == 0):
					valuedata = self.request.get(keydata)
					listsub.id = genid()
					listsub.in_apartmentid = idapart
					listsub.in_room = nroom
					listsub.serviceid = keydata
					listsub.price = float(valuedata)
					listsub.put()

				else:
					valuedata = self.request.get(keydata)
					if(valuedata == "True"):
						listsub.id = genid()
						listsub.in_apartmentid = idapart
						listsub.in_room = nroom
						listsub.serviceid = keydata
						listsub.price = 1
						listsub.put()
					else:
						pass

				time.sleep(1)
			qryroom = Room.query().filter(Room.id == nroom).fetch()
			for j in qryroom:
				j.submit_payment = 1
				j.put()
		elif(action == "fix_payment"):
			qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == idapart).fetch(1)
			qrydatepay = Apartment.query(Apartment.id == apartid).fetch()
			for itemdate in qrydatepay:
				datepay = itemdate.datepay
			for i in qrymonth:
				month_check = i.month
			d = datetime.utcnow()
			y = str(d.strftime("%Y"))
			if(int(month_check) == 1):
				timestart = str(int(y)-1)+"/"+str(12)+"/"+str(datepay)+" 17:00:00"
			else:
				timestart = y+"/"+str(int(month_check)-1)+"/"+str(datepay)+" 17:00:00"
			timeend = y+"/"+str(month_check)+"/"+'24'+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
			datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
			dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')
			qryser = Service.query().filter(Service.in_apartmentid == idapart).filter(Service.status == 1).filter(Service.type != 2).filter(Service.type != 3).fetch()
			for i in qryser:
				keydata = i.id
				valuedata = self.request.get(keydata)
				qrydefault =List_submitpayment.query().filter(List_submitpayment.in_apartmentid == idapart).filter(List_submitpayment.in_room == nroom).filter(List_submitpayment.serviceid == keydata).filter(ndb.AND(List_submitpayment.timestamp > datestart),(List_submitpayment.timestamp <= dateend)).fetch()
				self.response.out.write(str(len(qrydefault)))
				if(len(qrydefault) != 0):
					if(i.type == 0):
						for j in qrydefault:
							j.price = float(valuedata)
							j.timestamp = d
							j.put()
					else:
						if(valuedata == "False"):
							for j in qrydefault:
								j.key.delete()
				else:
					pass
					if(valuedata == "True"):
						listsub = List_submitpayment()
						listsub.id = genid()
						listsub.in_apartmentid = idapart
						listsub.in_room = nroom
						listsub.serviceid = keydata
						listsub.price = 1
						listsub.put()
						time.sleep(1)
class actionservice(webapp2.RequestHandler):

	def get(self):
		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		data = {'apartid':apartid}
		self.response.write(newservice.render(data))

	def post(self):
		action = self.request.get('action')
		apartmentid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartmentid
		if(action == 'e'):
			serviceid = self.request.get('sid')
			ntype = self.request.get('newstype')
			qry = Service.query().filter(Service.in_apartmentid == apartmentid and Service.id == serviceid )
			listser = qry.get()
			listser.name = self.request.get('newname')
			listser.price = float(self.request.get('newprice'))
			listser.type = int(ntype)
			listser.put()

		elif(action == 'c'):
			serviceid = self.request.get('sid')
			qry = Service.query().filter(Service.id == serviceid).filter(Service.in_apartmentid == apartmentid)

			chaser = qry.get()
			if(chaser.status == 1):
				chaser.status = 0
			else:
				chaser.status = 1
			chaser.put()

		elif(action == 'n'):
			name = self.request.get('nameser')
			price = self.request.get('price')
			stype = self.request.get('stype')
			ser = Service()
			ser.id = genid()
			ser.in_apartmentid = apartmentid
			ser.name = name
			if( int(stype) == 3):
				ser.price = float(1)
			else:
				ser.price = float(price)
			ser.type = int(stype)
			ser.put()
class weblogout(BaseHandler):
	def get(self):
		self.auth.unset_session()

		self.redirect(self.uri_for('loginpage'))
class reportfix(webapp2.RequestHandler):

	def get(self):
		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		qryroom = Room.query().filter(Room.in_apartmentid == apartid).fetch()
		qryfix = FixReport.query().filter(FixReport.in_apartmentid == apartid).fetch()
		data = {'qryfix':qryfix,'qryroom':qryroom}
		self.response.headers['Content-Type'] = 'text/html'
		self.response.write(pagefix.render(data))
	def post(self):
		reportid =self.request.get('id')
		qryreport = FixReport.query().filter(FixReport.id == reportid).fetch()
		for i in qryreport:
			i.status = 1
			i.put()

class forgetpass(webapp2.RequestHandler):
	def get(self):
		pass
class delall(webapp2.RequestHandler):
	def get(self):
		try:
			content = '<html><style>@font-face {font-family: THSarabunNew;src: url(fonts/THSarabunNew.ttf);}body {font-family: THSarabunNew;}table {font-family: THSarabunNew;}</style><head><title>Test PDF</title><meta http-equiv="content-type" content="text/html; charset=UTF-8"></head>หดเหกดเ</html>'
			output = StringIO()
			pdf = pisa.pisaDocument(content.encode("UTF-8"), output)
			pdf_data = pdf.dest.getvalue()

			self.response.headers['Content-Type'] = 'application/pdf'
			#self.response.headers['Content-Disposition'] = 'attachment;filename=Any_Name.pdf'
			self.response.write(pdf_data)
		except Exception, err:
			logging.info (traceback.format_exc())
class createinvoice(BaseHandler):
	@user_required
	def get(self):

		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		roomid = self.request.get('id')
		d = datetime.utcnow()
		year = str(d.strftime("%Y"))
		day = str(d.strftime("%d"))
		month = str(d.strftime("%m"))
		qrydatepay = Apartment.query(Apartment.id == apartid).fetch()
		for itemdate in qrydatepay:
			datepay = itemdate.datepay
		timestart = year+"/"+str(month)+"/"+str(datepay)+" 17:00:00"
		if(int(month) == 12):
			timeend = str(int(year)+1)+"/"+str(1)+"/"+str(datepay)+" 16:59:59"
		else:
			timeend = year+"/"+str(month)+"/"+str(datepay)+" 16:59:59"
		datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
		dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')
		if(datestart < d <dateend):
			#month
			#self.response.out.write(month)
			pass
		else:
			#month-1

			if(int(month) == 12):
				year = int(year)-1
			month = int(month)-1
			#self.response.out.write(month)
		"""
		{
		  "napart":"Skyview",
		  "billorder":"1235413134",
		  "ncus":"mond",
		  "room":"A101",
		  "list":
		  {
		    "power":{"unit":5,"use":7},
		    "water":{"unit":4,"use":20}
		  }
		}
		"""
		qryapart = Apartment.query().filter(Apartment.id == apartid).fetch(1)
		qrycus = Customer.query().filter(Customer.in_apartmentid == apartid).filter(Customer.in_roomid == roomid).fetch()
		qryroom = Room.query().filter(Room.id == roomid).fetch(1)
		jsonarray = {}
		tempcus = ""
		for room in qryroom:
			jsonarray['room'] = room.numroom
		for cus in qrycus:
			tempcus += str(cus.nname)+" "+str(cus.fname)+" "
		jsonarray['ncus'] = tempcus
		for apart in qryapart:
			jsonarray['napart'] = apart.name
		jsonarray['billorder'] = str(roomid)+str(int(month))+str(year)
		jsonlist = {}
		# jsonuse = {"unit":5,"use":7}
		# jsonlist['power'] = jsonuse
		# jsonlist['water'] = jsonuse
		#
		qryser = Service.query().filter(Service.in_apartmentid == apartid).filter(ndb.OR(Service.type == 0,Service.type==1))
		qrysum = List_sumpaymentroom.query().filter(List_sumpaymentroom.in_apartmentid == apartid).filter(List_sumpaymentroom.in_roomid == roomid)
		for sumlist in qrysum.fetch():
			if (sumlist.serviceid == "rent"):
				temp = {}
				temp['unit'] = 1.0
				temp['use'] = str(sumlist.price)
				jsonlist[str('ค่าห้อง')] = temp
			else:
				nameservice = qryser.filter(Service.id == sumlist.serviceid).fetch()
				for item in nameservice:
					temp = {}
					temp['unit'] = str(float(item.price))
					temp['use'] = str(float(sumlist.price/item.price))
					jsonlist[str(item.name)] = temp
		# for service in qryser:
		# 	idservice = service.id
		# 	qrysum = List_sumpaymentroom.query().filter(List_sumpaymentroom.in_apartmentid == apartid).filter(List_sumpaymentroom.in_room == rooid)
		# 	qryrent = jsonarray['rent']
		jsonarray['list'] = jsonlist
		url = "https://thelegend-dark.000webhostapp.com/sumpdf.php"
		data = jsonarray
		r = requests.post(url,data=json.dumps(data,cls=MyJsonEncoder,ensure_ascii=False)).content
		self.response.headers['Content-Type'] = 'application/pdf'
		self.response.out.write(r)
class servicepermonth(webapp2.RequestHandler):
	def post(self):
		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		action = self.request.get('action')
		try:
			if(action == "new"):
				name = self.request.get('name')
				unit = self.request.get('unit')
				ls = List_servicepermonth()
				ls.id = genid()
				ls.in_apartmentid = apartid
				ls.name = name
				ls.put()
				self.response.out.write("PASS")
			elif(action == "edit"):
				id = self.request.get('id')
				qry = List_servicepermonth.query().filter(ndb.AND(List_servicepermonth.in_apartmentid == apartid),(List_servicepermonth.id == id)).fetch()
				for i in qry:
					i.key.delete()
				self.response.out.write("PASS")
			elif(action == "submit"):
				qrypayment = List_servicepermonth.query().filter(List_servicepermonth.in_apartmentid == apartid).fetch()
				# self.response.out.write(str(len(qrypayment))+"<--a|")
				for item in qrypayment:
					keydata = item.id
					valuedata = self.request.get(keydata)
					# self.response.out.write(str(valuedata)+"|")
					ld = Ledger()
					ld.id = genid()
					ld.in_apartmentid = apartid
					ld.money = float(valuedata)
					ld.description = "รวม"+str(item.name)+"ทั้งหอพัก"
					ld.type = "OUTCOME"
					ld.put()
			else:
				self.response.out.write("NO")
		except:
			self.response.out.write("NO")
class graphyear(webapp2.RequestHandler):
	def get(self):
		# apartid = self.request.get('id')
		apartid = JINJA_ENVIRONMENT.globals['sessionapartid']
		JINJA_ENVIRONMENT.globals['sessionapartid'] = apartid
		qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == apartid).fetch(1)
		qrydatepay = Apartment.query(Apartment.id == apartid).fetch()
		for itemdate in qrydatepay:
			datepay = itemdate.datepay
		tempdatepay = datepay
		d = datetime.utcnow() + timedelta(hours=7)
		qrypayment = paymentmonth.query().filter(paymentmonth.in_apartmentid == apartid)
		for itempayment in qrypayment:
			month_check = int(itempayment.month)
		# month_check = int(d.strftime("%m"))
		y = int(d.strftime("%Y"))
		array = []
		countarray = 11
		for i in range(1,13):
			array.append(["",0,0])
		for countmonth in range(1,13):
			if(month_check == 0):
				month_check = 12
				y -= 1

			if(month_check == 2 and int(datepay) >28):
				datepay = 28
			else:
				datepay = tempdatepay

			if(month_check !=1):
				timestart = str(y)+"/"+str(int(month_check)-1)+"/"+str(datepay)+" 17:00:00"
			else:
				timestart = str(int(y)-1)+"/"+str(int(13)-1)+"/"+str(datepay)+" 17:00:00"
			timeend = str(y)+"/"+str(month_check)+"/"+str(datepay)+" 16:59:59"
			# self.response.out.write(timestart)


			# timeend = y+"/"+str(month_check)+"/"+'24'+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
			datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
			dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')
			qrysum = Ledger.query().filter(Ledger.in_apartmentid == apartid)
			sumoutcome = 0
			sumincome = 0
			for item in qrysum.filter(Ledger.type == "OUTCOME").filter(ndb.AND(Ledger.timestamp >=datestart,Ledger.timestamp <=dateend)):
				sumoutcome += item.money
			for item in qrysum.filter(Ledger.type == "INCOME").filter(ndb.AND(Ledger.timestamp >=datestart,Ledger.timestamp <=dateend)):
				sumincome += item.money
			array[countarray]= ([str(month_check)+"/"+str(y),sumincome,sumoutcome])
			month_check -=1
			countarray -=1
		a = json.dumps(array,cls=MyJsonEncoder,ensure_ascii=False)
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(a)
#MOBILE
class moblieservice(webapp2.RequestHandler):
	def post(self):
		json_data = self.request.body
		data = json.loads(json_data)
		apartid = data['apartid']
		qry = Service.query().filter(Service.in_apartmentid == apartid)
		datamonth  = []
		dataonce = []
		datapayment = []
		for p in qry.filter(Service.type != 2).filter(Service.type != 3):
			item = {"id": p.id,"in_apartmentid":p.in_apartmentid,"name":p.name,"type":p.type}
			datamonth.append(item)

		for p in qry.filter(Service.type != 0).filter(Service.type != 1):
			item = {"id": p.id,"in_apartmentid":p.in_apartmentid,"name":p.name,"type":p.type}
			dataonce.append(item)

		qry2 = Apartment.query().filter(Apartment.id == apartid).fetch()

		qry3 = paymentmonth.query().filter(paymentmonth.in_apartmentid == apartid).fetch()
		for pay in qry3:
			item = {"month":pay.month,"year":pay.year}
			datapayment.append(item)
		a = json.dumps({'ServiceMonth':datamonth,'ServiceOnce':dataonce,'Apartment':[p.to_dict() for p in qry2],'MonthYear':datapayment},cls=MyJsonEncoder,ensure_ascii=False)
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(a)

class Loginmoblie(webapp2.RequestHandler):
	"""docstring for tabservice"""
	def post(self):
		json_data = self.request.body
		data = json.loads(json_data)
		temp = jwt.decode(data['token'],'ArMzAdOg1')
		user = temp['id']
		password = temp['pass']
		deviceid = temp['deviceid']

		qry = Customer.query().filter(Customer.users == user)
		listuser = qry.get()
		check =0
		if(user != None and password != None and listuser != None):
			h_password = hashlib.md5(password)
			if(listuser.users == user and listuser.password == h_password.hexdigest()):

				qry2 = qry.fetch()
				for p in qry2:

					qrytoken = Customer_Token.query().filter(ndb.AND(Customer_Token.userid == listuser.id),(Customer_Token.deviceid == deviceid))
					if(len(qrytoken.fetch())==0):
						item = {"device":deviceid,"userid":listuser.id}
						data_encode = jwt.encode(item, 'ArMzAdOg2', algorithm='HS256')
						CT = Customer_Token()
						CT.id = genid()
						CT.token = data_encode
						CT.userid = listuser.id
						CT.deviceid = deviceid
						CT.put()
					else:
						for i in qrytoken:
							item = {"device":deviceid,"userid":listuser.id}
							break
						data_encode = jwt.encode(item, 'ArMzAdOg2', algorithm='HS256')

				self.response.out.write(data_encode)


				check = 1

			else:
				check =0

		if check == 0:
			self.response.headers['Content-Type'] = 'text/html'
			self.response.out.write("-1")
class checktoken(webapp2.RequestHandler):
	def post(self):
		json_data = self.request.body
		data = json.loads(json_data)
		token_verify = data['token']
		deviceid = data['device']
		qryuserid = Customer_Token.query().filter(ndb.AND(Customer_Token.token == token_verify),(Customer_Token.deviceid == deviceid))
		if(len(qryuserid.fetch())==0):
			self.response.out.write("-1")
		else:
			for item in qryuserid:
				userid = item.userid
				qry2 = Customer.query().filter(Customer.id == userid)
				data  = []
				for p in qry2:
					item = {"id": p.id,"in_apartmentid":p.in_apartmentid,"nname":p.nname,"address":p.address,"birth":str(p.birth),"lname":p.lname,"start_come":str(p.start_come),"tel":p.tel,"urlpic":p.urlpic,"fname":p.fname,"in_roomid":p.in_roomid,"users":p.users,"money":p.money}
					data.append(item)
				a = {'user':data}
				data_encode = jwt.encode(a, 'ArMzAdOg3', algorithm='HS256')
				self.response.out.write(data_encode)
class orderservice(webapp2.RequestHandler):
	def get(self):
		apartid = self.request.get('apartid')
		roomid = self.request.get('roomid')
		qry =  List_serviceRoom.query().filter(List_serviceRoom.in_apartmentid == apartid and List_serviceRoom.in_room == roomid).fetch()
		qryroom = Room.query().filter(Room.id == roomid).get()
		data  = []
		for p in qry:
			item = {"id": p.id,"in_apartmentid":p.in_apartmentid,"in_room":qryroom.numroom,"price":p.price,"service":p.service,"timestamp":p.timestamp,"type":p.type,"user":p.user}
			data.append(item)
		a = json.dumps(data,cls=MyJsonEncoder,ensure_ascii=False)
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(a)
class genstring(webapp2.RequestHandler):
	"""docstring for genstring"""
	def get(self):
		tmpstring = random_generator(50, "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
		hcheck = hashlib.md5(tmpstring).hexdigest()
		self.response.out.write(hcheck)
	def post(self):
		json_data = self.request.body
		data = json.loads(json_data)
		idphone = data['id']
		qrcode = QRcodegen()
		qry = QRcodegen.query().filter(QRcodegen.id_user == idphone)
		randomstr = ""
		check = 0
		randomstr = random_generator(50, "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
		tmpstring = idphone+"|"+randomstr
		hcheck = hashlib.md5(tmpstring).hexdigest()
		for i in qry:
			if(i.id_user == idphone):
				if(time_difference(i.timeexp) > 300):

					"""logs = Translog()
					logs.action = "Userid: "+idphone+" qrgenstring: "+randomstr
					logs.put()"""
					qryget = qry.get()
					qryget.string = randomstr
					qryget.hashcheck = hcheck
					qryget.timeexp = datetime.now()
					qryget.put()
					check = 1
					break
				else:
					check = 1
					hcheck = i.hashcheck

		if(check == 0):
			"""logs = Translog()
			logs.action = "Userid: "+idphone+" qrgenstring: "+randomstr
			logs.put()"""

			qrcode.id_user = idphone
			qrcode.string = randomstr
			qrcode.hashcheck = hcheck
			qrcode.put()
		output = hcheck
		self.response.out.write(output)
class mobliefix(webapp2.RequestHandler):
	def get(self):
		roomid = self.request.get('roomid')
		qryroom = Room.query().filter(Room.id == roomid).fetch()
		for l in qryroom:
			nameroom = l.numroom
		qryfix = FixReport.query().filter(FixReport.in_roomid == roomid).fetch()
		data = []
		for i in qryfix:
			item = {"room":nameroom,"description":i.description,"status":i.status}
			data.append(item)
		a = json.dumps({'Fix':data},cls=MyJsonEncoder,ensure_ascii=False)
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(a)
	def post(self):
		json_data = self.request.body
		data = json.loads(json_data)
		apartid = data['apartid']
		roomid = data['roomid']
		userid = data['userid']
		des = data['des']
		try:
			fix = FixReport()
			fix.id = genid()
			fix.in_apartmentid = apartid
			fix.in_roomid = roomid
			fix.in_userid = userid
			fix.description = des
			fix.put()
			self.response.out.write("1")
		except:
			self.response.out.write("0")
class mobilechangpass(webapp2.RequestHandler):
	def post(self):
		json_data = self.request.body

		try:
			data = json.loads(json_data)
			temp = jwt.decode(data['token'],'ArMzAdOg1')
			userid = temp['userid']
			oldpass = temp['oldpass']
			newpass = temp['newpass']
			hashpassold = hashlib.md5(oldpass).hexdigest()
			hashpassnew = hashlib.md5(newpass).hexdigest()
			qryuser = Customer.query().filter(ndb.AND(Customer.id == userid),(Customer.password == hashpassold)).fetch(1)
			if(len(qryuser) != 0):
				for i in qryuser:
					i.password = hashpassnew
					i.put()
				self.response.out.write("1")
			else:
				self.response.out.write("0")
		except:
			self.response.out.write("0")
class mobliereportmonth(webapp2.RequestHandler):
	"""docstring for mobliereportmonth."""
	def get(self):
		roomid = self.request.get('roomid')
		apartid = self.request.get('apartid')

		# date = self.request.get('date')
		# month,year = date.split("-")#04-2017
		# if(int(month) == 1):
		# 	timestart = str(int(year)-1)+"/"+str(12)+"/"+'24'+" 17:00:00"
		# else:
		# 	timestart = year+"/"+str(int(month)-1)+"/"+'24'+" 17:00:00"
		# timeend = year+"/"+str(month)+"/"+'24'+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
		# datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
		# dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')
		# qry = List_sumpaymentroom.query().filter(List_sumpaymentroom.in_apartmentid == apartid).filter(List_sumpaymentroom.in_roomid == roomid).filter(ndb.AND(List_submitpayment.timestamp > datestart),(List_submitpayment.timestamp <= dateend)).fetch()
		qry = List_sumpaymentroom.query().filter(List_sumpaymentroom.in_apartmentid == apartid).filter(List_sumpaymentroom.in_roomid == roomid)
		qryservice = Service.query().filter(Service.in_apartmentid == apartid).filter(Service.status == 1).filter(Service.type != 2).filter(Service.type != 3)
		data = []
		if (len(qry.fetch()) != 0):
			for listsum in qry:#List_sumpaymentroom
				templist = {}
				if (listsum.serviceid == "rent"):
					#self.response.out.write(str(listsum.id)+"|")
					# templist["ค่าห้อง"] = listsum.price
					unirent = unicode("ค่าห้อง")
					templist[unirent] = listsum.price
					temproomid = listsum.in_roomid
					temptime = listsum.timestamp.strftime("%m-%d-%Y")
					templiststart = str(temptime)+" 00:00:00"
					templistend = str(temptime)+" 23:59:59"
					timequerystart = datetime.strptime(templiststart, "%m-%d-%Y %H:%M:%S")
					timequeryend = datetime.strptime(templistend, "%m-%d-%Y %H:%M:%S")
					templist["timestamp"] = temptime
					qrybytime = qry.filter(ndb.AND(List_sumpaymentroom.timestamp >= timequerystart,List_sumpaymentroom.timestamp < timequeryend)).fetch()
					for item in qrybytime:
						tempserviceid = item.serviceid
						if(item.in_roomid == temproomid):
							qryservicebyid = qryservice.filter(Service.id == item.serviceid)
							for i in qryservicebyid:
								templist[i.name] = item.price
					data.append(templist)
					# for listtime in qry.filter(ndb.AND(List_sumpaymentroom.timestamp >= timequerystart,List_sumpaymentroom.timestamp < timequeryend)).fetch():
					#
					# 	if(listtime.serviceid != "rent"):
					# 		# self.response.out.write(len(qry.filter(ndb.AND(List_sumpaymentroom.timestamp >= timequerystart,List_sumpaymentroom.timestamp < timequeryend)).fetch()))
					# 		for listservice in qryservice.fetch():
					# 			if(listservice.id == listtime.serviceid):
					# 				templist[listservice.name] = listtime.price
					# 	else:
					# 		unirent = unicode("ค่าห้อง")
					# 		templist[unirent] = listtime.price
					# data.append(templist)
				# else:
				# 	otherlist = {}
				#
				# 	for listservice in qryservice:
				# 		if (listservice.id == listsum.serviceid):
				# 			# templist[listservice.name] = listsum.price
				# 			# templist[listservice.name] = listsum.price
				# 			otherlist[listservice.name] = listsum.price
				# 			#item = {listservice.name:listsum.price,"timestamp":listsum.timestamp}
				# 			#data.append(templist)
				# 			templist['list'] = otherlist
				# 			data.append(templist)
			a = json.dumps(data,cls=MyJsonEncoder,ensure_ascii=False)
			# # # a = json.dumps([p.to_dict() for p in qry],cls=MyJsonEncoder,ensure_ascii=False)
			self.response.headers['Content-Type'] = 'application/json'
			self.response.out.write(a)
		else:
			self.response.out.write("0")


#API Raspi
class payservice(webapp2.RequestHandler):
	def get(self):
		user = self.request.get('id')
		service = self.request.get('service')
		price = self.request.get('price')
		qry = Service.query().filter(Service.in_apartmentid == user).filter(ndb.AND(Service.status == 2),(Service.status == 3)).fetch()
		qry2 = Apartment.query().filter(Apartment.id == user).fetch()
		a = json.dumps({'Service':[p.to_dict() for p in qry],'Apartment':[p.to_dict() for p in qry2]},cls=MyJsonEncoder,ensure_ascii=False)
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(a)
class loginraspi(webapp2.RequestHandler):
	def get(self):
		user = self.request.get('id')
		password = self.request.get('password')
		action = self.request.get('action')
		hashpass = hashlib.md5(password)
		if(action == "login"):
			qry = AdminAccount.query().filter(AdminAccount.users == user)
			check =0
			for i in qry.fetch():
				if i.users == user and i.password == hashpass.hexdigest():
					qryser = Service.query().filter(Service.in_apartmentid == i.in_apartmentid).filter(Service.status == 1).filter(Service.type != 0).filter(Service.type != 1).fetch()
					qryapart = Apartment.query().filter(Apartment.id == i.in_apartmentid).fetch()
					a = json.dumps({'Service':[p.to_dict() for p in qryser],'Apartment':[p.to_dict() for p in qryapart]},cls=MyJsonEncoder,ensure_ascii=False)
					self.response.headers['Content-Type'] = 'application/json'
					self.response.out.write(a)
					check = 1
					break
				else:
					check =0

			if check == 0:
				self.response.out.write('-1')
		else:

			qryser = Service.query().filter(Service.in_apartmentid == user).filter(Service.status == 1).filter(Service.type != 0).filter(Service.type != 1).fetch()
			qryapart = Apartment.query().filter(Apartment.id == user).fetch()
			a = json.dumps({'Service':[p.to_dict() for p in qryser],'Apartment':[p.to_dict() for p in qryapart]},cls=MyJsonEncoder,ensure_ascii=False)
			self.response.headers['Content-Type'] = 'application/json'
			self.response.out.write(a)
class checkuserraspi(webapp2.RequestHandler):
	def post(self):
		qrcode = self.request.get('qrcode')
		qrycode = QRcodegen.query().filter(QRcodegen.hashcheck == qrcode)

		check = 0
		for i in qrycode:

			if(time_difference(i.timeexp) < 300):
				check = 1
				userid = i.id_user
				break
			else:
				check = 0

		if(check == 1):
			data  = []
			qryuser = Customer.query().filter(Customer.id == userid).fetch()
			if( len(qryuser) != 0):
				for p in qryuser:
					item = {"id": p.id,"users":p.users,"fname":p.fname,"lname":p.lname}
					data.append(item)
				a = json.dumps({'user':data},cls=MyJsonEncoder,ensure_ascii=False)
				self.response.headers['Content-Type'] = 'application/json'
				self.response.out.write(a)
			else:
				self.response.out.write('-2')
		else:
			self.response.out.write('-1')
class paymentraspi(webapp2.RequestHandler):
	def post(self):
		cusid = self.request.get('cusid')
		service = self.request.get('serid')
		qryser = Service.query().filter(Service.id == service)
		qrycus = Customer.query().filter(Customer.id == cusid)

		for i in qrycus:
			for j in qryser:
				ser_price = j.price
				ser_name = j.name
				ser_type = j.type
				if (ser_type == 3):
					cal_price = self.request.get('price')
					if(i.money-float(cal_price) < 0):
						self.response.out.write("Not Enough Money")
						break
					else:
						i.money -= float(cal_price)
						i.put()
						apartid = i.in_apartmentid
						roomid = i.in_roomid
						users = i.users
						LR = List_serviceRoom()
						LR.id = genid()
						LR.in_apartmentid = apartid
						LR.type = ser_type
						LR.in_room = roomid
						LR.user = users
						LR.service = ser_name
						LR.price = float(cal_price)
						LR.serviceid = service
						LR.put()
						self.response.out.write("Payment Finish")
						break
				else:
					if(i.money-float(ser_price) < 0):
						self.response.out.write("Not Enough Money")
						break
					else:
						i.money -= float(ser_price)
						i.put()
						apartid = i.in_apartmentid
						roomid = i.in_roomid
						users = i.users
						LR = List_serviceRoom()
						LR.id = genid()
						LR.in_apartmentid = apartid
						LR.type = ser_type
						LR.in_room = roomid
						LR.user = users
						LR.service = ser_name
						LR.price = float(ser_price)
						LR.serviceid = service
						LR.put()
						self.response.out.write("Payment Finish")
						break
# 		else:
# 			self.response.out.write("NO user")
# #firebase
class fbindex(webapp2.RequestHandler):
	def get(self):
		d = datetime.utcnow()
		month = int(d.strftime("%m"))
		self.response.out.write(str(month))


#cron
class cronupdateroom(webapp2.RequestHandler):
	def get(self):
		qryapart = Apartment.query()
		datepass = datetime.utcnow() + timedelta(days=-5,hours=7)
		getdate = datepass.strftime("%d")
		for i in qryapart:
			tempgetdate = i.datepay

			if(tempgetdate == getdate):
			# getdate + timedelta(days=-5,hours=7)
				# self.response.out.write(str(getdate))
				for i in qryapart:
					getdate = i.datepay

				qryroom = Room.query().filter(Room.submit_payment == 1)

				for i in qryroom:
					i.submit_payment = 0
					i.put()
		# datenow = datetime.utcnow() + timedelta(hours=7)
		# datepass = datetime.utcnow() + timedelta(days=-5,hours=7)



class cronupdatemonth(webapp2.RequestHandler):
	def get(self):
		qryapart = Apartment.query().fetch()
		datepass = datetime.utcnow() + timedelta(days=10,hours=7)
		getdate = datepass.strftime("%d")
		for i in qryapart:
			idapart = i.id
			nameapart = i.name
			datepay = i.datepay
			if (str(datepay) == getdate):
				qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == idapart).fetch(1)
				for m in qrymonth:
					month_check = m.month
				d = datetime.utcnow()
				y = str(d.strftime("%Y"))

				if(int(month_check) == 1):
					timestart = str(int(y)-1)+"/"+str(12)+"/"+str(datepay)+" 17:00:00"
				else:
					timestart = y+"/"+str(int(month_check)-1)+"/"+str(datepay)+" 17:00:00"
				timeend = y+"/"+str(month_check)+"/"+'24'+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
				datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
				dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')

				qry = List_sumpaymentroom.query().filter(List_sumpaymentroom.in_apartmentid == idapart).filter(ndb.AND(List_sumpaymentroom.timestamp > datestart),(List_sumpaymentroom.timestamp <= dateend))
				# qrysubmitpay =List_submitpayment.query().filter(ndb.AND(List_submitpayment.timestamp > datestart),(List_submitpayment.timestamp <= dateend)).filter(List_submitpayment.in_apartmentid == idapart)
				qryser = Service.query().filter(Service.in_apartmentid == idapart)
				for itemservice in qryser.fetch():
					nameservice = itemservice
					tempqry = qry.filter(List_sumpaymentroom.service == itemservice.id)
					if(len(tempqry.fetch() != 0)):
						summary = 0.0
						for itemsum in tempqry:
							summary += float(itemsum)
						ld = Ledger()
						ld.in_apartmentid = idapart
						ld.id = genid()+str(random_generator(5, "abcdefghijklmnopqrstuvwxyz0123456789"))
						ld.money = float(summary)
						ld.description = "รวม" + str(nameservice)
						ld.type = 'INCOME'
			# for service in qryser:
			# 	summary = 0
			# 	for submit in qrysubmitpay:
			# 		if(service.id == submit.serviceid):
			# 			summary += float(service.price*submit.price)
			# 			break
			#
			#
			# 	led = Ledger()
			# 	led.id = genid()
			# 	string = "รวม"
			# 	led.description = string+service.name
			# 	led.money = summary
			# 	led.type = "INCOME"
			# 	led.put()
class sumpaymentroom(webapp2.RequestHandler):
	def get(self):
		datenow = datetime.utcnow() + timedelta(hours=7)
		qryapartment = Apartment.query().filter(Apartment.datepay == str(datenow)).fetch()
		for apartment in qryapartment:
			idapart = apartment.id
			qryroom = Room.query().filter(Room.in_apartmentid == idapart).filter(Room.status == "1").filter(Room.used > 0).fetch()
			qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == idapart).fetch(1)
			for m in qrymonth:
				month_check = m.month
			d = datetime.utcnow()
			y = str(d.strftime("%Y"))
			qrydatepay = Apartment.query(Apartment.id == apartid).fetch()
			for itemdate in qrydatepay:
				datepay = itemdate.datepay
			if(int(month_check) == 1):
				timestart = str(int(y)-1)+"/"+str(12)+"/"+str(datepay)+" 17:00:00"
			else:
				timestart = y+"/"+str(int(month_check)-1)+"/"+str(datepay)+" 17:00:00"
			timeend = y+"/"+str(month_check)+"/"+str(datepay)+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
			datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
			dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')

			qryserviceapart = Service.query().filter(Service.in_apartmentid == idapart).filter(Service.status == 1).fetch()
			for serviceapart in qryserviceapart:
				idservice = serviceapart.id
				if (serviceapart.type == 2 or serviceapart.type == 3):
					for room in qryroom:
						roomid = room.id
						qryserviceroom = List_serviceRoom.query().filter(List_serviceRoom.in_apartmentid == idapart).filter(List_serviceRoom.in_room == roomid).filter(List_serviceRoom.serviceid == idservice).filter(ndb.AND(List_submitpayment.timestamp > datestart),(List_submitpayment.timestamp <= dateend)).fetch()
						summary = 0.0
						for listservice in qryserviceroom:
							summary += listservice.price
						lp = List_sumpaymentroom()
						lp.billorder = str(roomid+str(month_check)+y)
						lp.id = genid()+str(random_generator(5, "abcdefghijklmnopqrstuvwxyz0123456789"))
						lp.in_apartmentid = idapart
						lp.in_roomid = roomid
						lp.serviceid = idservice
						lp.price = float(summary)
						lp.put()
				else:
					for room in qryroom:
						roomid = room.id
						qryserviceroom = List_submitpayment.query().filter(List_submitpayment.in_apartmentid == idapart).filter(List_submitpayment.in_room == roomid).filter(List_submitpayment.serviceid == idservice).filter(ndb.AND(List_submitpayment.timestamp > datestart),(List_submitpayment.timestamp <= dateend)).fetch()
						summary = 0.0
						for listservice in qryserviceroom:
							summary += float(listservice.price*serviceapart.price)
						lp = List_sumpaymentroom()
						lp.billorder = str(roomid+str(month_check)+y)
						lp.id = genid()+str(random_generator(5, "abcdefghijklmnopqrstuvwxyz0123456789"))
						lp.in_apartmentid = idapart
						lp.in_roomid = roomid
						lp.serviceid = idservice
						lp.price = float(summary)
						lp.put()

			for listroom in qryroom:
				lp = List_sumpaymentroom()
				lp.billorder = str(roomid+str(month_check)+y)
				lp.id = genid()+str(random_generator(5, "abcdefghijklmnopqrstuvwxyz0123456789"))
				lp.in_apartmentid = idapart
				lp.in_roomid = listroom.id
				lp.serviceid = "rent"
				lp.price = float(listroom.rent)
				lp.put()
			qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == idapart).fetch()

			for i in qrymonth:
				tempmonth = i.month
				tempyear = i.year
				if(tempmonth+1 >12):
					tempmonth = 1
					tempyear += 1
				else:
					tempmonth += 1
				i.month = tempmonth
				i.year = tempyear
				i.put()

class testtoken(webapp2.RequestHandler):
	def get(self):
		# apartid = self.request.get('id')
		apartid = "280420171493401565"
		qrymonth = paymentmonth.query().filter(paymentmonth.in_apartmentid == apartid).fetch(1)
		qrydatepay = Apartment.query(Apartment.id == apartid).fetch()
		for itemdate in qrydatepay:
			datepay = itemdate.datepay
		tempdatepay = datepay
		d = datetime.utcnow() + timedelta(hours=7)
		month_check = int(d.strftime("%m"))
		y = int(d.strftime("%Y"))
		array = []
		countarray = 11
		for i in range(1,13):
			array.append(["",0,0])
		for countmonth in range(1,13):
			if(month_check == 0):
				month_check = 12
				y -= 1

			if(month_check == 2 and int(datepay) >28):
				datepay = 28
			else:
				datepay = tempdatepay

			if(month_check !=1):
				timestart = str(y)+"/"+str(int(month_check)-1)+"/"+str(datepay)+" 17:00:00"
			else:
				timestart = str(int(y)-1)+"/"+str(int(13)-1)+"/"+str(datepay)+" 17:00:00"
			timeend = str(y)+"/"+str(month_check)+"/"+str(datepay)+" 16:59:59"
			# self.response.out.write(timestart)


			# timeend = y+"/"+str(month_check)+"/"+'24'+" 16:59:59" # minus 7 hours = day if want day 5 at 23.59.59 fill 5 at 16.59.59
			datestart = datetime.strptime(timestart, '%Y/%m/%d %H:%M:%S')
			dateend = datetime.strptime(timeend, '%Y/%m/%d %H:%M:%S')
			qrysum = Ledger.query().filter(Ledger.in_apartmentid == apartid)
			sumoutcome = 0
			sumincome = 0
			for item in qrysum.filter(Ledger.type == "OUTCOME").filter(ndb.AND(Ledger.timestamp >=datestart,Ledger.timestamp <=dateend)):
				sumoutcome += item.money
			for item in qrysum.filter(Ledger.type == "INCOME").filter(ndb.AND(Ledger.timestamp >=datestart,Ledger.timestamp <=dateend)):
				sumincome += item.money
			array[countarray]= ([str(month_check)+"/"+str(y),sumincome,sumoutcome])
			month_check -=1
			countarray -=1
		a = json.dumps(array,cls=MyJsonEncoder,ensure_ascii=False)
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(a)
		# self.response.out.write(array)

			# # qrysubmitpay = List_submitpayment.query().filter(List_submitpayment.in_apartmentid == apartid).filter(ndb.AND(List_submitpayment.timestamp > datestart),(List_submitpayment.timestamp <= dateend)).fetch()
			# # # self.response.out.write("PASS")
			# # a = json.dumps({'user':[p.to_dict() for p in qrysubmitpay]},cls=MyJsonEncoder,ensure_ascii=False)
			# # self.response.headers['Content-Type'] = 'application/json'
			# # self.response.out.write(a)
			# qryapart = Apartment.query().filter(Apartment.id == apartid)
			# datepass = datetime.utcnow() + timedelta(days=-5,hours=7)
			# getdate = datepass.strftime("%d")
			# for i in qryapart:
			# 	tempgetdate = i.datepay

			# 	if(tempgetdate == getdate):
			# 	# getdate + timedelta(days=-5,hours=7)
			# 		self.response.out.write(str(getdate))
			# 	else:
			# 		pass

		# self.response.out.write(str(datenow))
config = {
  'webapp2_extras.auth': {
	'user_model': 'models.User',
	'user_attributes': ['email_address']
  },
  'webapp2_extras.sessions': {
	'secret_key': 'd5NKWEtv3Z',
	'session_max_age':900
  }
}


app = webapp2.WSGIApplication([
	webapp2.Route('/loginpage', LoginPage,name='loginpage'),
	webapp2.Route('/login', Login,name='login'),
	webapp2.Route('/regis', Register),
	webapp2.Route('/regisnew', Regisaccount),
	webapp2.Route('/',Dashboard,name='main'),
	webapp2.Route('/regiscus',regiscus),
	webapp2.Route('/tabservice',Consoleadmin),
	webapp2.Route('/newroom',NewRooms),
	webapp2.Route('/testinput',testinput),
	webapp2.Route('/superadmin',superadmin),
	webapp2.Route('/changepass',changepass),
	webapp2.Route('/querypic',showpic),
	webapp2.Route('/forgetpass',forgetpass),
	webapp2.Route('/actionservice',actionservice),
	webapp2.Route('/maindashboard',maindashboard),
	webapp2.Route('/actionroom',actionroom),
	webapp2.Route('/invoice',createinvoice),
	webapp2.Route('/weblogout',weblogout,name='logout'),
	webapp2.Route('/delall',delall),
	webapp2.Route('/send/fix',reportfix),
	webapp2.Route('/actioncus',actioncus),
	webapp2.Route('/payservice',payservice),
	webapp2.Route('/graphyear',graphyear),
	webapp2.Route('/servicepermonth',servicepermonth),
	webapp2.Route('/mobile/genstring',genstring),
	webapp2.Route('/mobile/loginmoblie',Loginmoblie),
	webapp2.Route('/mobile/checktoken',checktoken),
	webapp2.Route('/mobile/service',moblieservice),
	webapp2.Route('/mobile/order',orderservice),
	webapp2.Route('/mobile/reportfix',mobliefix),
	webapp2.Route('/mobile/reportmonth',mobliereportmonth),
	webapp2.Route('/mobile/changepass',mobilechangpass),
	webapp2.Route('/raspi/login',loginraspi),
	webapp2.Route('/raspi/checkuser',checkuserraspi),
	webapp2.Route('/raspi/payment',paymentraspi),
	webapp2.Route('/cron/updatepaymentroom',cronupdateroom),
	webapp2.Route('/cron/cronupdatemonth',cronupdatemonth),
	webapp2.Route('/cron/sumpaymentroom',sumpaymentroom),
	webapp2.Route('/firebase/index',fbindex),


	webapp2.Route('/testtoken',testtoken)
], debug=True,config=config)
