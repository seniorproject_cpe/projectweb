from google.appengine.ext import ndb


class Apartment(ndb.Model):
	"""docstring for Room"""
	id = ndb.StringProperty(required=True)
	name = ndb.StringProperty(required=True)
	description = ndb.StringProperty(required=True)
	telephone = ndb.StringProperty(required=True)
	datepay = ndb.StringProperty(required=True)

class List_serviceRoom(ndb.Model):
	id = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(default="")
	in_room = ndb.StringProperty(default="")
	serviceid = ndb.StringProperty(default="")
	user = ndb.StringProperty(default="")
	service = ndb.StringProperty(default="")
	price = ndb.FloatProperty(default=0)
	type = ndb.IntegerProperty(default=1)
	timestamp = ndb.DateTimeProperty(auto_now_add=True)

class List_userinroom(ndb.Model):
	username = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(required=True)
	in_roomid = ndb.StringProperty(required=True)

class List_adminControl(ndb.Model):
	in_apartmentid = ndb.StringProperty(required=True)
	username = ndb.StringProperty(required=True)

class List_submitpayment(ndb.Model):
	id = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(required=True)
	in_room = ndb.StringProperty(default="")
	serviceid = ndb.StringProperty(default="")
	price = ndb.FloatProperty(default=0)
	timestamp = ndb.DateTimeProperty(auto_now_add=True)

class List_servicepermonth(ndb.Model):
	id = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(required=True)
	name = ndb.StringProperty(required=True)

class List_sumpaymentroom(ndb.Model):
	id = ndb.StringProperty(required=True)
	billorder = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(required=True)
	in_roomid = ndb.StringProperty(required=True)
	serviceid = ndb.StringProperty(required=True)
	price = ndb.FloatProperty(default=0)
	timestamp = ndb.DateTimeProperty(auto_now_add=True)
