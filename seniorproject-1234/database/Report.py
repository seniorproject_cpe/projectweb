from google.appengine.ext import ndb

class FixReport(ndb.Model):
    """docstring for Room"""
    id = ndb.StringProperty(required=True)
    in_apartmentid = ndb.StringProperty(required=True)
    in_roomid = ndb.StringProperty(required=True)
    in_userid = ndb.StringProperty(required=True)
    description = ndb.StringProperty()
    status = ndb.IntegerProperty(default = 0)
    timestamp = ndb.DateTimeProperty(auto_now_add=True)
