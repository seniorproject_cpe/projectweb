from google.appengine.ext import ndb

class AdminAccount(ndb.Model):
	id = ndb.StringProperty(required=True)
	users = ndb.StringProperty(required=True)
	password = ndb.StringProperty(required=True)
	name = ndb.StringProperty(required=True)
	typeuser = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(required=True)
