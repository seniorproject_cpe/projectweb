from google.appengine.ext import ndb


class Ledger(ndb.Model):
	"""type = INCOME or OUTCOME"""
	id = ndb.StringProperty(required=True)
	in_apartmentid = ndb.StringProperty(required=True)
	money = ndb.FloatProperty(required=True)
	description = ndb.StringProperty(required=True)
	type = ndb.StringProperty(required=True)
	timestamp = ndb.DateProperty(auto_now=True)
